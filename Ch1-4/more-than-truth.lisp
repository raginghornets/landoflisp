;; Check if 1 is in the list (3 4 1 5)
(if (member 1 '(3 4 1 5))
    'one-is-in-the-list
    'one-is-not-in-the-list)

;; Returns (1 5)
(member 1 '(3 4 1 5))

;; Returns (4 1 5)
(member 4 '(3 4 1 5))

;; Returns (nil)
;; Because it returns a list, it always evaluates to true
(member nil '(3 4 1 nil))

;; Returns 5
(find-if #'oddp '(2 4 5 6))

;; Returns nil because there is no null value
(find-if #'null '(2 4 5 6))

;; Returns nil... because there is a null value? Wut. This is so sad.
(find-if #'null '(2 4 nil 6))