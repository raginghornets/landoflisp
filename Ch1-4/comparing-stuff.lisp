;; eq is the simplest and fastest comparison function
;; however it does not compare by "looks"
;; so its generally not used for conses

(defparameter *fruit* 'apple)

(cond ((eq *fruit* 'apple) 'its-an-apple)
      ((eq *fruit* 'orange) 'its-an-orange))

;; If not dealing with symbols, use equal

;; comparing symbols
(equal 'apple 'apple)

;; comparing lists
(equal (list 1 2 3) (list 1 2 3))

;; identical lists created in different ways still compare as the same
(equal '(1 2 3) (cons 1 (cons 2 (cons 3))))

;; comparing integers
(equal 5 5)

;; comparing floating point numbers
(equal 2.5 2.5)

;; comparing strings
(equal "foo" "foo")

;; comparing characters
(equal #\a #\a)

;; eql is similar to eq, but also
;; compares numbers and characters
(eql 'foo 'foo)
(eql 3.4 3.4)
(eql #\a #\a)

;; equalp is basically same as equal
;; except it also does fancy stuff
(equalp "Bob Smith" "bob smith") ;; T
(equalp 0 0.0) ;; T