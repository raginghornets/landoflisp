;; (and) and (or) can be used as conditionals

;; (if) example
(if *file-modified*
    (if (ask-user-about-saving)
        (save-file)))

;; (and) example
(and *file-modified* (ask-user-about-saving) (save-file))

;; (if) and (and) example
(if (and *file-modified* (ask-user-about-saving))
    (save-file))