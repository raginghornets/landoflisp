(defun say-hello ()
    (print "Please type your name:")
    (let ((name (read)))
        (print "Nice to meet you, ")
        (print name)))

;; Note how the print function uses raw user input.
;; That is, it doesn't treat all user input as strings.
(defun add-five ()
    (print "please enter a number:")
    (let ((num (read)))
        (print "When I add five I get")
        (print (+ num 5))))

(print '3) ;; An integer
(print '3.4) ;; A float

;; A symbol. It may be printed in all caps, since
;; Common Lisp symbols are blind to letter case.
(print 'foo)

(print '"foo") ;; A string
(print '#\a) ;; A character

;; =======================
;; I/O FOR HUMANS
;; =======================

(princ '3); => 3
(princ '3.4); => 3.4
(princ 'foo); => FOO
(princ '"foo"); => foo
(princ '#\a); => a

(progn
    (princ "This sentence will be interrupted")
    (princ #\newline)
    (princ "by an annoying newline character."))

(defun say-hello ()
    (princ "Please type your name: ")
    (let ((name (read-line)))
        (princ "Nice to meet you, ")
        (princ name)))

;; Use eval on data to turn it to code
;; CAUTION: it is powerful and dangerous!
(defparameter *foo* '(+ 1 2))
(eval *foo*)

