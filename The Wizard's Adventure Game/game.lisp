;; Define the locations and their descriptions
(defparameter *nodes* '(
    (living-room (you are in the living-room. a wizard is snoring loudly on the couch.))
    (garden (you are in a beautiful garden. there is a well in front of you.))
    (attic (you are in the attic. there is a giant welding torch in the corner.))))

;; Return the description of a location from a given set of nodes
(defun describe-location (location nodes)
    (cadr (assoc location nodes)))

;; Define the mediums with which we can change locations
(defparameter *edges* '(
    (living-room (garden west door) (attic upstairs ladder))
    (garden (living-room east door))
    (attic (living-room downstairs ladder))))

;; Return the description of a given edge
(defun describe-path (edge)
    `(there is a ,(caddr edge) going ,(cadr edge) from here.))

;; Return a concatenated description of every edge available at a given location
(defun describe-paths (location edges)
    (apply #'append (mapcar #'describe-path (cdr (assoc location edges)))))

;; Define list of visible objects
(defparameter *objects* '(whiskey bucket frog chain))

;; Define list of object locations
(defparameter *object-locations* '(
    (whiskey living-room)
    (bucket living-room)
    (chain garden)
    (frog garden)))

;; Return list of visible objects at a given location
(defun objects-at (loc objs obj-locs)
    (labels 
        ((at-loc-p (obj) (eq (cadr (assoc obj obj-locs)) loc)))
        (remove-if-not #'at-loc-p objs)))

;; Return a concatenated description of every visible object at a given location
(defun describe-objects (loc objs obj-loc)
    (labels
        ((describe-obj (obj) `(you see a ,obj on the floor.)))
        (apply #'append (mapcar #'describe-obj (objects-at loc objs obj-loc)))))

;; Define the current location of the player
(defparameter *location* 'living-room)

;; --------------------------------------------------
;; NON-FUNCTIONAL TERRITORY
;; --------------------------------------------------

;; Return a description of the player's current location
(defun look ()
    (append
        (describe-location *location* *nodes*)
        (describe-paths *location* *edges*)
        (describe-objects *location* *objects* *object-locations*)))

;; If the direction is valid, adjust the player's location
;; and return a description of the new location
;; Else, the direction is invalid and return an admonishment
(defun walk (direction)
    (let
        ((next (find direction
            (cdr (assoc *location* *edges*))
            :key #'cadr)))
    (if next
        (progn
            (setf *location* (car next))
            (look))
        '(you cannot go that way.))))

;; If object is available at the current location, pick up the object
;; and return a message notifying that the object has been picked up
;; Else, the object is unavailable at the current location,
;; and return an admonishment
(defun pickup (object)
    (cond 
        ((member object (objects-at *location* *objects* *object-locations*))
            (push (list object 'body) *object-locations*)
            `(you are now carrying the ,object))
        (t '(you cannot get that))))

;; Return an inventory of objects that the player is carrying
(defun inventory ()
    (cons 'items- (objects-at 'body *objects* *object-locations*)))

;; Custom read to make it easier for the user to type commands
;; Ex: (walk 'west) would be: walk west
(defun game-read ()
    (let

        ;; Concatenate the user input with parenthesis
        ((cmd (read-from-string (concatenate 'string "(" (read-line) ")"))))

        ;; Allow local functions to be defined (simpler version of labels)
        (flet

            ;; Define local function quote-it that returns x as data, if it is code
            ((quote-it (x) (list 'quote x)))

            ;; Apply quote-it to the command except for the
            ;; beginning and then join the quoted and unquoted part
            (cons (car cmd) (mapcar #'quote-it (cdr cmd))))))

(defparameter *allowed-commands* '(look walk pickup inventory))

;; If the command is valid, then return its evaluation
;; Else, return an adonishment
;; Note: it is not 100% secure
(defun game-eval (sexp)
    (if (member (car sexp) *allowed-commands*)
        (eval sexp)
        '(i do not know that command)))

;; Helper function of game-print that recursively
;; corrects the text in a character list
(defun tweak-text (lst caps lit)
    (when lst
        (let (
            (item (car lst))
            (rest (cdr lst)))
        (cond
            ((eq item #\space)
                (cons item (tweak-text rest caps lit)))
            ((member item '(#\! #\? #\.))
                (cons item (tweak-text rest t lit)))
            ((eq item #\")
                (tweak-text rest caps (not lit)))
            (lit (cons item (tweak-text rest nil lit)))
            ((or caps lit)
                (cons (char-upcase item) (tweak-text rest nil lit)))
            (t (cons (char-downcase item) (tweak-text rest nil nil)))))))

;; Coerces the tweaked character list into one proper string
;; and princs it, also adding a fresh line at the end
(defun game-print (lst)
    (princ
        (coerce
            (tweak-text
                (coerce (string-trim "() " (prin1-to-string lst)) 'list) t nil)
            'string))
    (fresh-line))

;; Custom recursive REPL to allow for easier commands
(defun game-repl ()
    (let ((cmd (game-read)))
        (unless (eq (car cmd) 'quit)
            (game-print (game-eval cmd))
            (game-repl))))