# The Wizard's Adventure Game

## Overview

There are three locations which players may vist: the garden, the living room, and the attic. Players can move between locations using the door and the ladder to the attic.

### Basic Requirements
- Looking around
- Walking to different locations
- Picking up objects
- Performing actions on the objects picked up

#### Looking Around
- Basic scenery
- One or more paths to other locations
- Objects that you can pick up and manipulate